# irobotcreate2_description package

This package contains the dynamic model of the iRobotCreate2 "Centro Piaggio" robot. 

Compiling
---
Just clone the repository in the src folder of a catkin workspace. Then run catkin_make.

---
To simulate only one robot in a Gazebo empty world run:

```
roslaunch irobotcreate2_description one_robot_gazebo.launch 
```